from django.shortcuts import render, HttpResponse
from .models import Contacto

def hola_mundo(request):

    contexto = {
        'title': ':: Hola Mundo de Docker',
        'contactos': Contacto.objects.all()
    }
    return render(request,'base.html',contexto)
